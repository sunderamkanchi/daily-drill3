function cardIssueBeforeJune(cardData) {
  let result = cardData.filter((beforeJune) => {
    let monthData = beforeJune.issue_date;
    let newData = new Date(monthData);
    let monthFilter = newData.getMonth();
    if (monthFilter < 5) {
      return beforeJune;
    }
  });
  return result;
}
module.exports = cardIssueBeforeJune;
