function addCcvData(ccvData) {
  let result = ccvData
    .map((cardDetails) => {
      let ccvRandom = Math.ceil(Math.random() * 1000);
      //console.log(ccvRandom);
      cardDetails["ccv"] = ccvRandom;
      return cardDetails;
    })
    .map((addValidation) => {
      addValidation["status"] = "valid";
      return addValidation;
    })
    .map((validationstatus) => {
      let monthData = validationstatus.issue_date;
      let newData = new Date(monthData);
      let monthFilter = newData.getMonth();
      if (monthFilter < 3) {
        validationstatus.status = "invalid";
        return validationstatus;
      } else {
        return validationstatus;
      }
    })
    .sort((a, b) => {
      if (new Date(a.issue_date) < new Date(b.issue_date)) {
        return -1;
      } else {
        return 1;
      }
    })
    .reduce((acc, curr) => {
      let month = [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December",
      ];
      let dateOther = curr.issue_date;
      let date = new Date(dateOther);
      let monthOther = date.getMonth();
      let monthData = month[monthOther];
      if (monthData !== acc) {
        acc[monthData] = [];
      }
      acc[monthData].push(curr);
      return acc;
    }, {});
  return result;
}
module.exports = addCcvData;
