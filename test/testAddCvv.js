let fs = require("fs");
let path = require("path");

let data = require("../p3");
let addCcvData = require("../addCcv");
let result = addCcvData(data);
console.log(result);

fs.writeFile(
  path.join(__dirname, "result.json"),
  JSON.stringify(result),
  (error) => {
    if (error) {
      return console.log(error);
    }
    console.log("file Data stored ");
  }
);
